import datetime
import os
import sys

sys.path.insert(0, os.path.abspath('..'))
sys.path.insert(0, os.path.abspath('../..'))

date = datetime.datetime.now().date()
release = os.getenv('CI_PIPELINE_IID', 'latest')
project = 'nestlog'
author = 'Sol Courtney'
copyright = f'{date!s}, {author!s}'
extensions = ['sphinx.ext.napoleon', 'sphinx.ext.autodoc']
html_theme = 'sphinx_rtd_theme'
napoleon_include_special_with_doc = True
napoleon_include_private_with_doc = False
add_module_names = False
