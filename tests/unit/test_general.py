"""General unit test module."""

import unittest

import nestlog


class TestGeneral(unittest.TestCase):

    """General unit test cases."""

    def test_import(self):
        """Test imports."""
        self.assertIsNotNone(nestlog)

    def test_logger_1(self):
        """Make sure the logger works."""
        logger = nestlog.NestLogger()
        with logger('test'):
            logger.okay('hey')
            logger.warn('hey')
            logger.fail('hey')
            with logger('test-2'):
                logger.okay('hey')
                logger.warn('hey')
                logger.fail('hey')
                with logger('test-3'):
                    logger.okay('hey')
                    logger.warn('hey')
                    logger.fail('hey')
                    with logger('test-4'):
                        logger.okay('hey')
                        logger.warn('hey')
                        logger.fail('hey')

    def test_logger_2(self):
        """Make sure the logger works."""
        logger = nestlog.NestLogger(1)
        with logger('test'):
            logger.okay('hey')
            logger.warn('hey')
            logger.fail('hey')
            with logger('test-2'):
                logger.okay('hey')
                logger.warn('hey')
                logger.fail('hey')
                with logger('test-3'):
                    logger.okay('hey')
                    logger.warn('hey')
                    logger.fail('hey')
